export DOCKER_CLIENT_TIMEOUT=6000
export COMPOSE_HTTP_TIMEOUT=6000

docker compose --env-file .env up -d --force-recreate --remove-orphans transmission-openvpn
